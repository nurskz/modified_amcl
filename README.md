
# Modified AMCL package

[![pipeline status](https://gitlab.com/nurskz/modified_amcl/badges/master/pipeline.svg)](https://gitlab.com/nurskz/modified_amcl/commits/master)
[![codecov](https://codecov.io/gl/nurskz/modified_amcl/branch/master/graph/badge.svg?token=M3KGZHWHYW)](https://codecov.io/gl/nurskz/modified_amcl)
<!-- [![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) -->


## What is this?

This package is the modified version of existing AMCL localization package to be more robust to large scene changes caused by temporal, but predictable objects (e.g., curtains, opened/closed doors).  For example, in the image below, the hospital curtains cause scene changes due to their different states (e.g., opened, half-opened, and closed). 

<img src="docs/img/beds_curtains.png" alt="Beds and curtains" align="left" style="zoom:60%;" />

Sometimes, such changes can negatively impact on the the localization behavior. The purpose of added modification to the existing `nav2_amcl` package  is to provide relatively better 2D localization for autonomous vehicles in an indoor environment.  As a prerequisite, the predictable objects are manually indicated in the 2D laser-based map by highlighting them with gray color as follows:

<img src="docs/img/map_modification.png" alt="Before and after map modification" align="left" style="zoom:90%;" />

As a result, the localization behavior notable improved as shown in below:

<img src="docs/img/existingVSmodified.png" alt="Existing vs modified amcl" align="left" style="zoom:90%;" />



## Scope

This module is the modified version of the existing [nav2_amcl](https://github.com/ros-planning/navigation2/tree/foxy-devel/nav2_amcl) localization package of the ROS2 navigation stack. Consequently, it has the same dependencies as the original `nav_amcl` package. Besides, it is dependent and expected to run together with the ROS2 navigation stack ([foxy-devel](https://github.com/ros-planning/navigation2/tree/foxy-devel)).

As mentioned earlier, the predictable objects are manually indicated in the 2D map. Hence, the `nav2_map_server` package should also be modified to take into account the predictable objects when loading the modified map. For this reason, we have only modified `map_io.cpp` as follows: 

```c++
      double delta = 0.02;  // ccl chages

      int8_t map_cell;
      switch (load_parameters.mode) {
        case MapMode::Trinary:
          if (load_parameters.occupied_thresh < occ) {
            map_cell = 100;
          } else if (occ < load_parameters.free_thresh) {
            map_cell = 0;
          } else if (occ > (0.5 - delta) && occ < (0.5 + delta)) {  // ccl chages
            map_cell = 50;  // if occ value is around 50, i.e. pixel value=128; (255-128)/255~=0.5
          } else {
            map_cell = -1;
          }
          break;
        case MapMode::Scale:
          if (pixel.alphaQuantum() != OpaqueOpacity) {
            map_cell = -1;
          } else if (load_parameters.occupied_thresh < occ) {
            map_cell = 100;
          } else if (occ < load_parameters.free_thresh) {
            map_cell = 0;
          } else if (occ > (0.5 - delta) && occ < (0.5 + delta)) {  // ccl chages
            map_cell = 50;  // if occ value is around 50, i.e. pixel value=128; (255-128)/255~=0.5
          } else {
            map_cell = std::rint(
              (occ - load_parameters.free_thresh) /
              (load_parameters.occupied_thresh - load_parameters.free_thresh) * 100.0);
          }
          break;
```

where lines of code with comments are the added modifications (lines 212-213, 221-222, 234-235). The rest of the package is kept as it is.

As for `nav2_amcl`, the modifications are only done inside of `amcl_node.cpp`. In particular, the following part of code was added (lines 1215-1217): 

```c++
  // Convert to player format
  for (int i = 0; i < map->size_x * map->size_y; i++) {
    if (map_msg.data[i] == 0) {
      map->cells[i].occ_state = -1;
    } else if (map_msg.data[i] == 100) {
      map->cells[i].occ_state = +1;
      // treat all gray pixel (value==50) to be considered as map feature as well
    } else if (map_msg.data[i] == 50) {
      map->cells[i].occ_state = +1;  // ediited by raymond and wenchao
    } else {
      map->cells[i].occ_state = 0;
    }
  }
```

The rest of the package is kept as it is.

## Tested versions of ROS

This module is tested with ROS2 Foxy on Ubuntu 20.04.

## Setup

This module is dependent and expected to run together with the ROS2 navigation stack ([foxy-devel](https://github.com/ros-planning/navigation2/tree/foxy-devel)). Hence, make sure that ROS2 navigation stack ([foxy-devel](https://github.com/ros-planning/navigation2/tree/foxy-devel)) is built and functional with its own ROS2 overlay workspace. For detailed instructions, check https://navigation.ros.org/build_instructions/index.html#manually-build-main. Do not forget to build for foxy-devel distribution. At the end you should have two ROS2 overlay workspaces:  `nav2_depend_ws` and `nav2_ws`.

Next we need to download the modified versions

```
# Create ROS2 workspace
cd $HOME

# Download copy of modied_amcl
cd gazebo_world_ros2_ws/src
git clone https://gitlab.com/nurskz/modified_amcl.git
cd modified_amcl
```

Replace the existing `nav2_amcl` and `nav2_map_server` packages in `nav2_ws` with the modified versions in `modified_amcl` folder. Do not forget to `colcon build` ROS2 navigation stack `nav2_ws` again.

## Run

Now you can perform your tests in `nav2_ws` with modified `nav2_amcl` package. Please do not forget that it is required to manually highlight the predictable objects in the 2D laser-based map with gray color each time if you update your map.
