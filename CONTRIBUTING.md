Contributions to existing files should be made under the license of that file.
New files should be made under the first license listed in the appropriate
package.xml file